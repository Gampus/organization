# Challenge 1

Deadline: 2024-05-16 12:00 MEZ

## Before we start: Set up your development environment

You will need to do some coding for these challenges. We expect you to know your way around your developement environment
and how to implement basic applications.

Here's a handy list to start. Please spend some time getting familar with how to use these tools. Don't be like [Michael Scott](https://youtu.be/xTQ7vhtp23w?si=bqMbKhvMmz1ANAGQ&t=48).

- Enroll in our Slack. We sent an invitation link to your DHBW Mannheim mail address.
- Create a [Gitlab](https://gitlab.com) account and post your username in the corresponding Slack thread for us to give you access to your team's Gitlab group.
- Install [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git). Running `git --version` in a terminal should then return the installed version of Git.
- Install [any code editor](https://github.com/zeelsheladiya/Awesome-IDEs) (IDE).
- Install [Docker](https://docs.docker.com/engine/install/).

> Help, I'm using a company device and have no rights to install these tools.

Please try to get your hands on a private device where you have administrator privileges.
If that's not possiblem, some of the tools we use might be available through your company's software delivery center.

If that's also not the case, you could use some kind of virtual machine in the cloud.
Some cloud providers offer free development machines that come pre-installed with Git and IDEs which you can use in your browser, for example [Google Cloud Shell](https://cloud.google.com/shell/docs/launching-cloud-shell).

## Part 1 [Presentation]: Create a high-level system architecture (10 pts)

You are IT consultants currently working for a large german bank. Your customer asks you to come up with an architecture for a greenfield large-scale banking IT landscape.

> Greenfield means that you don't need to worry about existing systems and infrastructure. Everything needs to be done from scratch.

In this first iteration of the architectural design, think about what components you would introduce to meet the customer's requirements (see requirements below).

Draw an architectural diagram (you can use UML diagrams if you like to).
Write a document that explains your architecture. Try to answer:

- What data resources are there (for example customer accounts)?
- What (micro)services would you introduce and what are their responsibilities?
- How do these services play together to implement the business processes (for example, what happens when a customer opens an account)?
- What trade-offs did you make (if any) and justify your decision making.

```
Functional requirements:

- The system can store the amount of money of customers in their bank accounts.
- The system can store customer data such as name, address, etc.
- The system can serve multiple end-user facing clients (Online Banking Website, Mobile App, ATMs)
- Customers can post transactions that transfer money from one bank account to another.
- New customers can open new accounts through the Online Banking.
- The system generates a monthly income statement for each customer.

Non-functional requirements:
- Customer data and customer accounts are the responsibility of different organizational domains and they need to be able to develop these systems indepdently.
- Data consistency is of utmost importance for accounts and their transactions. There must be no margin of error when dealing with account values.

Out of scope:
- Don't worry about databases or data models right now. You only need to think about what data objects there are and where they get maintained.
- Don't make any assumptions about the underlying infrastructure yet.
```

> Remember, this is a thinking exercise. There is no right or wrong solution here. We want you to become creative and think about potential solutions.

Prepapre a form-free 5 minute presentation of your architecture for our next session and be ready to answer questions about it.

## Part 2 [Coding]: Implement a Trading API (10 pts)

A few years ago, we hosted a trading bot competition at DHBW. Located at `challenges/challenge1/trading-api.yaml` is the (reduced) OpenAPI specification for our Paper Trading API.
We want you to create an webservice that implements this API. It does not need to store data in a database, in-memory is sufficient for now.
For example, if a client sends a valid request to the `POST /orders` endpoint, the API should return the response documented in the OpenAPI specification filled with actual values:

```
{
  "orderID": "123456789",
  "assetType": "CRYPTO",
  "symbol": "BTC",
  "amount": 1,
  "side": "BUY",
  "orderType": "MARKET",
  "quote": 1123.22,
  "total": 2246.44,
  "placedAt": "2021-02-01T18:07:00"
}
```

The API should return real values for the fields, so the `placedAt` fields needs to contain the actual timestamp of when the order has been created
and the `quote` should include the actual current market price for the asset.

For Cryptocurrency prices, you could choose from the following list of providers:

```
- Binance: Live-Daten für 100+ Kryptowährungen.
- Kraken: Live-Daten für 50+ Kryptowährungen.
- Coinmarketcap: Neartime-Daten für 8000+ Kryptowährungen von 300+ Börsen.
- Coingecko: Live-Daten für 3000+ Kryptowährungen von 400+ Börsen.

(from https://gitlab.com/19amb/mammon/organisation/-/blob/main/RESOURCES.md)
```

If the client now sends a request to the `GET /orders` endpoint, the API should return all orders that have been created so far.

Containerize your webservice by providing a Dockerfile that builds a Docker image which - when run - starts your webservice.

Useful tools:
- Visualize the OpenAPI specification with [Swagger Editor](https://editor.swagger.io/) (just copy and paste it into there)
- [Postman](https://www.postman.com/) to make HTTP requests against your API

You are free to create the webservice with whatever programming language and framework you like. For example:
- [Node.js](https://nodejs.org/en) with [hapi](https://hapi.dev/) (recommended)
- [Python](https://www.python.org/) with [Flask](https://flask.palletsprojects.com/en/3.0.x/quickstart/)
- [Go](https://go.dev/) with [Gin-Gonic](https://gin-gonic.com/)

Create a new repository called `challenge1` in your Gitlab group and commit and push your webservice.
Provide a concise and helpful README file (you could get some inspiration on [makeareadme.com](https://www.makeareadme.com/)).
It must include clear instructions on how to run the Docker container of the service. (We can't rate what we can't test).

Within the next few days we will provide you with a way to automatically test whether your implementation fully adheres to the provided OpenAPI specification.
