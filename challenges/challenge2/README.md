# Challenge 2

## Part 1: Select databases for the banking architecture (10 pts)

Deadline: 2024-05-21 12:00 MESZ

In this next iteration of the architectural design, the bank asks you to decide what databases to use for the services.

Extend your architectural diagram so that it includes the databases used by the system.
Show what kind of database (relational database, Document DB, key-value stores, fileshares, etc.) which component(s) would use.

Prepapre a form-free 5 minute presentation of your database decisions for our next session and be ready to answer questions about it.

## Part 2: Implement database for Trading API (10 pts)

Deadline: 2024-05-23 12:00 MESZ

Your Trading API from challenge 2 needs a database to be able to store orders.
Additionally you want to test the ability to scale out, which means to run multiple stateless Trading API instances in parallel to be able to serve more concurrent requests.
Your setup will look like this:

![Trading API Architecture](challenges/challenge2/trading-api-architecture.png)

Implement the connection to the database in your Trading API and store orders that get created by the clients.

The database should be containerized.
Use [Docker Compose](https://docs.docker.com/compose/compose-application-model/) to configure a setup of two Trading API instances (getting mounted to the host system on ports `3000` and `3001`) and one database instance.
Executing `docker compose up` should start all of these components.

We have updated the Trading API tests which now will create an order on one Trading API instance and then read all orders on the second instance.

Choose a database that in your opinion is suited to store the orders.
For example, you can use databases such as:
- [Redis](https://redis.io/docs/install/install-redis/install-redis-on-windows/) (blazing fast key-value store)
- [MongoDB](https://www.mongodb.com/docs/manual/installation/) (Document database)
- [Dragonfly](https://www.dragonflydb.io/docs/getting-started) (key-value store)
- [MySQL](https://dev.mysql.com/doc/refman/8.3/en/installing.html) (relational database)
- [PostgreSQL](https://www.postgresql.org/download/) (relational database)

Fork the repository for your solution of the last challenge and create a new repository called `challenge2` for this challenge.
